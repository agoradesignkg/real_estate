<?php

namespace Drupal\real_estate\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides banner block.
 *
 * @Block(
 *   id = "property_pagination",
 *   admin_label = @Translation("Property pagination block"),
 *   category = @Translation("Real estate")
 * )
 */
class PropertyPaginationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $current_route;

  /**
   * The property repository.
   *
   * @var \Drupal\real_estate\Storage\PropertyStorageInterface
   */
  protected $propertyStorage;

  /**
   * Constructs a PropertyPaginationBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route
   *   The current route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $current_route) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->current_route = $current_route;
    $this->propertyStorage = $entity_type_manager->getStorage('re_property');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($this->current_route->getRouteName() != 'entity.re_property.canonical') {
      return [];
    }
    /** @var \Drupal\real_estate\Entity\PropertyInterface $property */
    $property = $this->current_route->getParameter('re_property');

    $prev = $this->propertyStorage->getPrevious($property);
    $next = $this->propertyStorage->getNext($property);
    $output['#theme'] = 'property_pagination';
    $output['#prev_url'] = $prev ? $prev->toUrl() : '';
    $output['#next_url'] = $next ? $next->toUrl() : '';
    $output['#overview_url'] = real_estate_get_properties_overview_url();
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['re_property_list']);
  }

}
