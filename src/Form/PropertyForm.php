<?php

namespace Drupal\real_estate\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the add/edit form for real estate property entities.
 */
class PropertyForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['price']['#states'] = [
      'visible' => [
        'select[name="contract_type"]' => ['value' => 'k'],
      ],
    ];

    $rent_dependent_fields = [
      'rent',
      'total_rent',
      'deposit',
    ];
    foreach ($rent_dependent_fields as $rent_dependent_field) {
      $form[$rent_dependent_field]['#states'] = [
        'visible' => [
          'select[name="contract_type"]' => [
            ['value' => 'm'],
            ['value' => 'mk'],
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    /** @var \Drupal\real_estate\Entity\PropertyInterface $entity */
    $entity = $this->getEntity();

    $this->messenger()->addStatus($this->t('The property %label has been successfully saved.', ['%label' => $entity->label()]));
    $form_state->setRedirect('entity.re_property.canonical', ['re_property' => $entity->id()]);

    return $result;
  }

}
