<?php

namespace Drupal\real_estate\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the real estate property type form.
 */
class PropertyTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\real_estate\Entity\PropertyTypeInterface $property_type */
    $property_type = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $property_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $property_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\real_estate\Entity\PropertyType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = $this->entity->save();
    $this->messenger()->addMessage($this->t('The property %label has been successfully saved.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect('entity.re_property_type.collection');
    return $result;
  }

}
