<?php

namespace Drupal\real_estate\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the real estate property type entity.
 *
 * @ConfigEntityType(
 *   id = "re_property_type",
 *   label = @Translation("Property type", context = "real_estate"),
 *   label_collection = @Translation("Property types", context = "real_estate"),
 *   label_singular = @Translation("Property type", context = "real_estate"),
 *   label_plural = @Translation("Property types", context = "real_estate"),
 *   label_count = @PluralTranslation(
 *     singular = "@count property type",
 *     plural = "@count property types",
 *     context = "real_estate"
 *   ),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "list_builder" = "Drupal\real_estate\ListBuilder\PropertyTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\real_estate\Form\PropertyTypeForm",
 *       "edit" = "Drupal\real_estate\Form\PropertyTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "re_property_type",
 *   admin_permission = "administer re_property_type",
 *   bundle_of = "re_property",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/property-types/add",
 *     "edit-form" = "/admin/structure/property-types/{re_property_type}/edit",
 *     "delete-form" = "/admin/structure/property-types/{re_property_type}/delete",
 *     "collection" = "/admin/structure/property-types"
 *   }
 * )
 */
class PropertyType extends ConfigEntityBundleBase implements PropertyTypeInterface {

  /**
   * The type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The type label.
   *
   * @var string
   */
  protected $label;

}
