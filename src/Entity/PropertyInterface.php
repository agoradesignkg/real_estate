<?php

namespace Drupal\real_estate\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the real estate property interface.
 */
interface PropertyInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the property title.
   *
   * @return string
   *   The property title.
   */
  public function getTitle();

  /**
   * Sets the property title.
   *
   * @param string $title
   *   The property title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Gets the object ID.
   *
   * @return string
   *   The object ID.
   */
  public function getObjectId();

  /**
   * Sets the object ID.
   *
   * @param string $object_id
   *   The object ID.
   *
   * @return $this
   */
  public function setObjectId($object_id);

  /**
   * Gets the description.
   *
   * @return string
   *   The description
   */
  public function getDescription();

  /**
   * Sets the description.
   *
   * @param string $description
   *   The description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the summary (short description).
   *
   * @return string
   *   The summary (short description)
   */
  public function getSummary();

  /**
   * Sets the summary (short description).
   *
   * @param string $summary
   *   The summary (short description).
   *
   * @return $this
   */
  public function setSummary($summary);

  /**
   * Gets the location.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The location term, or null.
   */
  public function getLocation();

  /**
   * Sets the location.
   *
   * @param \Drupal\taxonomy\TermInterface $location
   *   The location term.
   *
   * @return $this
   */
  public function setLocation(TermInterface $location);

  /**
   * Gets the location ID.
   *
   * @return int
   *   The location ID.
   */
  public function getLocationId();

  /**
   * Sets the location ID.
   *
   * @param int $location_id
   *   The location ID.
   *
   * @return $this
   */
  public function setLocationId($location_id);

  /**
   * Gets the property type.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The property type term, or null.
   */
  public function getPropertyType();

  /**
   * Sets the property type.
   *
   * @param \Drupal\taxonomy\TermInterface $property_type
   *   The property type term.
   *
   * @return $this
   */
  public function setPropertyType(TermInterface $property_type);

  /**
   * Gets the property type ID.
   *
   * @return int
   *   The property type ID.
   */
  public function getPropertyTypeId();

  /**
   * Sets the property type ID.
   *
   * @param int $property_type_id
   *   The property type ID.
   *
   * @return $this
   */
  public function setPropertyTypeId($property_type_id);

  /**
   * Gets the contract type (key).
   *
   * @return string
   *   The contract type - the actual value that is stored within the database,
   *   not the label that is normally shown to end users.
   */
  public function getContractType();

  /**
   * Sets the contract type.
   *
   * @param string $contract_type
   *   The contract type.
   *
   * @return $this
   */
  public function setContractType($contract_type);

  /**
   * Gets the contract type label.
   *
   * @return string
   *   The contract type label - as it is normally shown to end users.
   */
  public function getContractTypeLabel();

  /**
   * Gets the living area in m2.
   *
   * @return float
   *   The living area in m2.
   */
  public function getLivingArea();

  /**
   * Sets the living area in m2.
   *
   * @param float $living_area
   *   The living area in m2.
   *
   * @return $this
   */
  public function setLivingArea($living_area);

  /**
   * Gets the plot area in m2.
   *
   * @return float
   *   The plot area in m2.
   */
  public function getPlotArea();

  /**
   * Sets the plot area in m2.
   *
   * @param float $plot_area
   *   The plot area in m2.
   *
   * @return $this
   */
  public function setPlotArea($plot_area);

  /**
   * Gets the build year.
   *
   * @return int
   *   The build year.
   */
  public function getBuildYear();

  /**
   * Sets the build year.
   *
   * @param int $build_year
   *   The build year.
   *
   * @return $this
   */
  public function setBuildYear($build_year);

  /**
   * Gets the heating energy demand (HWB).
   *
   * @return float
   *   The heating energy demand (HWB).
   */
  public function getHeatingEnergyDemand();

  /**
   * Sets the heating energy demand (HWB).
   *
   * @param float $heating_energy_demand
   *   The heating energy demand (HWB).
   *
   * @return $this
   */
  public function setHeatingEnergyDemand($heating_energy_demand);

  /**
   * Gets the total energy efficiency factor (fGEE).
   *
   * @return float
   *   The total energy efficiency factor (fGEE).
   */
  public function getTotalEnergyEfficiencyFactor();

  /**
   * Sets the total energy efficiency factor (fGEE).
   *
   * @param float $total_energy_efficiency_factor
   *   The total energy efficiency factor (fGEE).
   *
   * @return $this
   */
  public function setTotalEnergyEfficiencyFactor($total_energy_efficiency_factor);

  /**
   * Gets the heating system.
   *
   * @return string
   *   The heating system.
   */
  public function getHeatingSystem();

  /**
   * Sets the heating system.
   *
   * @param string $heating_system
   *   The heating system.
   *
   * @return $this
   */
  public function setHeatingSystem($heating_system);

  /**
   * Gets the available from time.
   *
   * @return string
   *   The available from time.
   */
  public function getAvailableFrom();

  /**
   * Sets the available from time.
   *
   * @param string $available_from
   *   The available from time.
   *
   * @return $this
   */
  public function setAvailableFrom($available_from);

  /**
   * Gets the price (in EUR).
   *
   * @return int
   *   The price (in EUR).
   */
  public function getPrice();

  /**
   * Sets the price (in EUR).
   *
   * @param int $price
   *   The price (in EUR).
   *
   * @return $this
   */
  public function setPrice($price);

  /**
   * Get the creation timestamp.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Set the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}
