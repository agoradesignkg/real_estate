<?php

namespace Drupal\real_estate\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining property type config entities.
 */
interface PropertyTypeInterface extends ConfigEntityInterface {

}
