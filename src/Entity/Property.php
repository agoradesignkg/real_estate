<?php

namespace Drupal\real_estate\Entity;

use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\taxonomy\TermInterface;
use Drupal\user\UserInterface;

/**
 * Defines the real estate property entity.
 *
 * @ContentEntityType(
 *   id = "re_property",
 *   label = @Translation("Property", context = "real_estate"),
 *   label_collection = @Translation("Properties", context = "real_estate"),
 *   label_singular = @Translation("property", context = "real_estate"),
 *   label_plural = @Translation("properties", context = "real_estate"),
 *   label_count = @PluralTranslation(
 *     singular = "@count property",
 *     plural = "@count properties",
 *     context = "real_estate"
 *   ),
 *   bundle_label = @Translation("Property type", context = "real_estate"),
 *   handlers = {
 *     "storage" = "Drupal\real_estate\Storage\PropertyStorage",
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\real_estate\ListBuilder\PropertyListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\real_estate\Form\PropertyForm",
 *       "add" = "Drupal\real_estate\Form\PropertyForm",
 *       "edit" = "Drupal\real_estate\Form\PropertyForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer re_property",
 *   permission_granularity = "bundle",
 *   translatable = TRUE,
 *   base_table = "re_property",
 *   data_table = "re_property_field_data",
 *   entity_keys = {
 *     "id" = "property_id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/property/{re_property}",
 *     "add-page" = "/property/add",
 *     "add-form" = "/property/add/{re_property_type}",
 *     "edit-form" = "/property/{re_property}/edit",
 *     "delete-form" = "/property/{re_property}/delete",
 *     "collection" = "/admin/content/properties"
 *   },
 *   bundle_entity_type = "re_property_type",
 *   field_ui_base_route = "entity.re_property_type.edit_form",
 * )
 */
class Property extends ContentEntityBase implements PropertyInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectId() {
    return $this->get('object_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setObjectId($object_id) {
    $this->set('object_id', $object_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return $this->get('summary')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSummary($summary) {
    $this->set('summary', $summary);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocation() {
    return $this->get('location')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocation(TermInterface $location) {
    $this->set('location', $location->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationId() {
    return $this->get('location')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocationId($location_id) {
    $this->set('location', $location_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyType() {
    return $this->get('property_type')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setPropertyType(TermInterface $property_type) {
    $this->set('property_type', $property_type->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyTypeId() {
    return $this->get('property_type')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPropertyTypeId($property_type_id) {
    $this->set('property_type', $property_type_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContractType() {
    return $this->get('contract_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setContractType($contract_type) {
    $this->set('contract_type', $contract_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContractTypeLabel() {
    $contract_type = $this->getContractType();
    if ($contract_type) {
      $values = real_estate_allowed_contract_type_values();
      $contract_type = $values[$contract_type];
    }
    return $contract_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getLivingArea() {
    return $this->get('living_area')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLivingArea($living_area) {
    $this->set('living_area', $living_area);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlotArea() {
    return $this->get('plot_area')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlotArea($plot_area) {
    $this->set('plot_area', $plot_area);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBuildYear() {
    return $this->get('build_year')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBuildYear($build_year) {
    $this->set('build_year', $build_year);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeatingEnergyDemand() {
    return $this->get('heating_energy_demand')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHeatingEnergyDemand($heating_energy_demand) {
    $this->set('heating_energy_demand', $heating_energy_demand);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalEnergyEfficiencyFactor() {
    return $this->get('total_energy_efficiency_factor')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTotalEnergyEfficiencyFactor($total_energy_efficiency_factor) {
    $this->set('total_energy_efficiency_factor', $total_energy_efficiency_factor);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeatingSystem() {
    return $this->get('heating_system')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHeatingSystem($heating_system) {
    $this->set('heating_system', $heating_system);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableFrom() {
    return $this->get('available_from')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAvailableFrom($available_from) {
    $this->set('available_from', $available_from);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice() {
    return $this->get('price')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPrice($price) {
    $this->set('price', $price);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->getEntityKey('owner');
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $key = $this->getEntityType()->getKey('owner');
    $this->set($key, $uid);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    $key = $this->getEntityType()->getKey('owner');
    return $this->get($key)->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $key = $this->getEntityType()->getKey('owner');
    $this->set($key, $account);

    return $this;
  }

  /**
   * Default value callback for 'owner' base field.
   *
   * @return mixed
   *   A default value for the owner field.
   */
  public static function getDefaultEntityOwner() {
    return \Drupal::currentUser()->id();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields[$entity_type->getKey('owner')] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('User ID'))
      ->setSetting('target_type', 'user')
      ->setTranslatable($entity_type->isTranslatable())
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner');

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['object_id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Object ID'))
      ->setTranslatable(FALSE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['property_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Property type', [], ['context' => 'real_estate']))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['property_types' => 'property_types'],
        'sort' => ['field' => '_none'],
        'auto_create' => FALSE,
        'auto_create_bundle' => '',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 2,
        'settings' => [
          'link' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['contract_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Contract type', [], ['context' => 'real_estate']))
      ->setRequired(TRUE)
      ->setSetting('max_length', 64)
      ->setSetting('allowed_values_function', 'real_estate_allowed_contract_type_values')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['location'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Location', [], ['context' => 'real_estate']))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['locations' => 'locations'],
        'sort' => ['field' => '_none'],
        'auto_create' => FALSE,
        'auto_create_bundle' => '',
      ])
      ->setDisplayOptions('form', [
        'type' => 'cshs',
        'weight' => 4,
        'settings' => [
          'parent' => 0,
          'level_labels' => '',
          'force_deepest' => FALSE,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 4,
        'settings' => [
          'link' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['summary'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Summary', [], ['context' => 'real_estate']))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'basic_string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(new TranslatableMarkup('Description', [], ['context' => 'real_estate']))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['position'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Position', [], ['context' => 'real_estate']))
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['living_area'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Living area'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setSetting('suffix', ' m²')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 8,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 8,
        'label' => 'inline',
        'settings' => [
          'thousands_separator' => '.',
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['plot_area'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Plot area'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setSetting('suffix', ' m²')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 9,
        'label' => 'inline',
        'settings' => [
          'thousands_separator' => '.',
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['rooms'] = BaseFieldDefinition::create('integer')
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setLabel(new TranslatableMarkup('Number of rooms'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 10,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '',
          'prefix_suffix' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['build_year'] = BaseFieldDefinition::create('integer')
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1600)
      ->setSetting('max', 2040)
      ->setLabel(new TranslatableMarkup('Build year'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 11,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 11,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '',
          'prefix_suffix' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['heating_energy_demand'] = BaseFieldDefinition::create('float')
      ->setLabel('HWB')
      ->setDescription(new TranslatableMarkup('Heating energy demand'))
      ->setSetting('suffix', ' kWh/m²a')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 12,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_decimal',
        'weight' => 12,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '.',
          'decimal_separator' => ',',
          'scale' => 2,
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['total_energy_efficiency_factor'] = BaseFieldDefinition::create('float')
      ->setLabel('fGEE')
      ->setDescription(new TranslatableMarkup('Total energy efficiency factor'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 13,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_decimal',
        'weight' => 13,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '.',
          'decimal_separator' => ',',
          'scale' => 2,
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['heating_system'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Heating'))
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 14,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['available_from'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Available from'))
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 15,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['operating_costs'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Operating costs'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setSetting('suffix', ' EUR')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 16,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 16,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '.',
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['rent'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Rent', [], ['context' => 'real_estate amount']))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setSetting('suffix', ' EUR')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 17,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 17,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '.',
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['total_rent'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Total rent', [], ['context' => 'real_estate']))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setSetting('suffix', ' EUR')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 18,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 18,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '.',
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['price'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Price'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setSetting('suffix', ' EUR')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 19,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 19,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '.',
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['deposit'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Deposit', [], ['context' => 'real_estate']))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setSetting('suffix', ' EUR')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 20,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '.',
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['commission'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Commission', [], ['context' => 'real_estate']))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1)
      ->setSetting('suffix', ' EUR')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 21,
      ])
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'weight' => 21,
        'label' => 'inline',
        'settings' => [
          'thousand_separator' => '.',
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['media'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Media'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'media')
      ->setSetting('handler', 'default:media')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'image' => 'image',
          'video' => 'video',
        ],
        'sort' => ['field' => '_none'],
        'auto_create' => FALSE,
        'auto_create_bundle' => '',
      ])
      ->setDisplayOptions('form', [
        'type' => 'media_library_widget',
        'weight' => 12,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'weight' => 12,
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setTranslatable(TRUE);

    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}
