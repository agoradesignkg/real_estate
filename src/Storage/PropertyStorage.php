<?php

namespace Drupal\real_estate\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\real_estate\Entity\PropertyInterface;

/**
 * The default storage for real estate property entities.
 *
 * @see \Drupal\real_estate\Entity\PropertyInterface
 */
class PropertyStorage extends SqlContentEntityStorage implements PropertyStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getPrevious(PropertyInterface $property) {
    $query = $this->getQuery()->condition('status', 1);
    $query->condition('property_id', $property->id(), '<');
    $query->sort('property_id', 'DESC');
    $query->range(0, 1);
    $query_result = $query->execute();
    $result = NULL;
    if ($query_result) {
      $id = reset($query_result);
      $result = $this->load($id);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getNext(PropertyInterface $property) {
    $query = $this->getQuery()->condition('status', 1);
    $query->condition('property_id', $property->id(), '>');
    $query->sort('property_id', 'ASC');
    $query->range(0, 1);
    $query_result = $query->execute();
    $result = NULL;
    if ($query_result) {
      $id = reset($query_result);
      $result = $this->load($id);
    }
    return $result;
  }

}
