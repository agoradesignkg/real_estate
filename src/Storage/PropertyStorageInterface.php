<?php

namespace Drupal\real_estate\Storage;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\real_estate\Entity\PropertyInterface;

/**
 * Defines the interface for real estate property storage classes.
 *
 * @see \Drupal\real_estate\Entity\PropertyInterface
 */
interface PropertyStorageInterface extends EntityStorageInterface {

  /**
   * Gets the previous property for the given one.
   *
   * Previous is here meant in terms of property ID.
   *
   * @param \Drupal\real_estate\Entity\PropertyInterface $property
   *   The property entity.
   *
   * @return \Drupal\real_estate\Entity\PropertyInterface|null
   *   The previous property or NULL.
   */
  public function getPrevious(PropertyInterface $property);

  /**
   * Gets the next property for the given one.
   *
   * Next is here meant in terms of property ID.
   *
   * @param \Drupal\real_estate\Entity\PropertyInterface $property
   *   The property entity.
   *
   * @return \Drupal\real_estate\Entity\PropertyInterface|null
   *   The next property or NULL.
   */
  public function getNext(PropertyInterface $property);

}
